#!/usr/bin/env python3

import argparse
from kiutils.board import Board
from kiutils.items.schitems import HierarchicalSheetInstance, \
                                   SymbolInstance, Rectangle
from kiutils.items.fpitems import FpLine, FpCircle, FpArc, \
                                  FpRect, FpPoly
from kiutils.schematic import Schematic
import logging
from pathlib import Path
import re
import shutil


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class SCH:
    V6_SCH_VERSION = "20211123"

    def __init__(self, sch_path: Path):
        self.schematic = Schematic.from_file(sch_path)  # type: ignore

    def sync(self) -> None:
        self.schematic.to_file(self.schematic.filePath)
        self.schematic = Schematic.from_file(self.schematic.filePath)  # type: ignore

    def sheet_instance_agg(self, recurse: bool = True) -> list[HierarchicalSheetInstance]:
        sheet_instances = self.schematic.sheetInstances.copy()
        for sheet in self.schematic.sheets:
            for instance in sheet.instances:  # instance: HierarchicalSheetProjectInstance
                for path in instance.paths:  # path: HierarchicalSheetProjectPath
                    sheet_instances.append(HierarchicalSheetInstance(instancePath=f"{path.sheetInstancePath}/{sheet.uuid}",
                                                                     page=path.page))
            if recurse:
                sheet_instances.extend(SCH(self.schematic.filePath.parent / sheet.fileName.value).sheet_instance_agg())  # type: ignore
            for si in sheet_instances:
                si.instancePath = re.sub(f"{self.schematic.uuid}/", "", si.instancePath)
        return sheet_instances

    def symbol_instance_agg(self, recurse: bool = True) -> list[SymbolInstance]:
        # FIXME: explicitly convert symbolInstances to list[SymbolInstance]
        symbol_instances = self.schematic.symbolInstances.copy()  # symbol_instances: list[SymbolInstance]
        for symbol in self.schematic.schematicSymbols:
            for instance in symbol.instances:  # instance: SymbolProjectInstance
                for path in instance.paths:  # path: SymbolProjectPath
                    symbol_instances.append(SymbolInstance(path=f"{path.sheetInstancePath}/{symbol.uuid}",
                                                           reference=path.reference,
                                                           unit=path.unit,
                                                           **{k: property.value for k in ('value', 'footprint')
                                                              for property in symbol.properties
                                                              if property.key.lower() == k}))
        for sheet in recurse and self.schematic.sheets or []:
            symbol_instances.extend(SCH(self.schematic.filePath.parent / sheet.fileName.value).symbol_instance_agg())  # type: ignore
        for si in symbol_instances:
            si.path = re.sub(f"{self.schematic.uuid}/", "", si.path)
        return symbol_instances

    def sheet_instances_set(self, sheetInstances: list[HierarchicalSheetInstance]) -> None:
        self.schematic.sheetInstances = sheetInstances
        self.sync()

    def symbol_instances_set(self, symbolInstances: list[SymbolInstance]) -> None:
        self.schematic.symbolInstances = symbolInstances
        self.sync()

    def sheet_instance_clear(self, recurse: bool = True) -> None:
        self.schematic.sheetInstances = []
        for sheet in recurse and self.schematic.sheets or []:
            sheet.instances = []
            SCH(self.schematic.filePath.parent / sheet.fileName.value).sheet_instance_clear()  # type: ignore
        self.sync()

    def symbol_instance_clear(self, recurse: bool = True) -> None:
        self.schematic.symbolInstances = []
        for symbol in self.schematic.schematicSymbols:
            symbol.instances = []
        for sheet in recurse and self.schematic.sheets or []:
            SCH(self.schematic.filePath.parent / sheet.fileName.value).symbol_instance_clear()  # type: ignore
        self.sync()

    def symbol_prop_dnp_clear(self, recurse: bool = True) -> None:
        for symbol in self.schematic.schematicSymbols:
            symbol.dnp = None
        for sheet in recurse and self.schematic.sheets or []:
            SCH(self.schematic.filePath.parent / sheet.fileName.value).symbol_prop_dnp_clear()  # type: ignore
        self.sync()

    def sheet_prop_rename(self, recurse: bool = True) -> None:
        for sheet in self.schematic.sheets:
            sheet.sheetName.key = "Sheet name"
            sheet.sheetName.id = 0
            sheet.fileName.key = "Sheet file"
            sheet.fileName.id = 1
            if recurse:
                SCH(self.schematic.filePath.parent / sheet.fileName.value).sheet_prop_rename()  # type: ignore
        self.sync()

    def rectangle_remove(self, recurse: bool = True) -> None:
        self.schematic.shapes = [shape for shape in self.schematic.shapes if not isinstance(shape, Rectangle)]
        self.schematic.to_file(self.schematic.filePath)
        for sheet in recurse and self.schematic.sheets or []:
            SCH(self.schematic.filePath.parent / sheet.fileName.value).rectangle_remove()  # type: ignore
        self.sync()

    def version_change(self, version=None, recurse: bool = True) -> None:
        self.schematic.version = version or self.V6_SCH_VERSION
        for sheet in recurse and self.schematic.sheets or []:
            SCH(self.schematic.filePath.parent / sheet.fileName.value).version_change()  # type: ignore
        self.sync()


class PCB:
    V6_PCB_VERSION = "20211014"

    def __init__(self, pcb_path: Path):
        # preemptively squash 'hide' in (model ...) expressions; not yet supported by kiutils
        with pcb_path.open('r+') as f:
            content = f.read()
            f.seek(0)
            f.write(re.sub(r"(^[ ]{4}\(model \".+\") (hide)", r"\1", content))
            f.truncate()
        self.board = Board.from_file(pcb_path)  # type: ignore

    def sync(self) -> None:
        self.board.to_file(self.board.filePath)
        self.board = Board.from_file(self.board.filePath)  # type: ignore

    def version_change(self, version=None) -> None:
        self.board.version = version or self.V6_PCB_VERSION
        self.sync()

    def fp_stroke_width_swap(self) -> None:
        for footprint in self.board.footprints:
            for fp in [gi for gi in footprint.graphicItems
                       if isinstance(gi, (FpLine, FpCircle, FpArc, FpRect, FpPoly))]:
                if fp.width is None:
                    fp.width = getattr(fp.stroke, 'width', None)
                    fp.stroke = None
        self.sync()


def run():
    parser = argparse.ArgumentParser(description='Downgrade KiCad schematic and board files from v7 to v6.')
    parser.add_argument('--input', '-i',
                        nargs=1, type=str,
                        default=None,
                        dest="input",
                        help="Path to a KiCad project.")
    parser.add_argument('--output', '-o',
                        nargs=1, type=str,
                        default=None,
                        dest="output",
                        help="Path to a directory to store the converted files.")

    args = parser.parse_args()
    logger.info(args.input)
    if args.input is None:
        raise ValueError("Specify an input path.")
    elif not (input_path := Path(args.input[0])).exists():
        raise ValueError("Input path does not exist.")
    elif input_path.is_file() and input_path.suffix == '.kicad_pro':
        project_name = input_path.stem
        project_path = input_path.parent
    elif input_path.is_dir():
        project_name = input_path.stem
        project_path = input_path
    else:
        raise ValueError("Input path should be a path to a .kicad_pro file or to a directory containing a project.")
    logger.info(f"{project_name=}")
    logger.info(f"{project_path=}")
    if not len(input_files := list(project_path.glob('*.kicad_[sp]c[hb]'))):
        raise ValueError("No .kicad_sch or .kicad_pcb files were found.")

    if args.output is None:
        raise ValueError("Specify an output path.")
    else:
        output_path = Path(args.output[0])
        output_path.mkdir(parents=True, exist_ok=False)
    logger.info(f"{output_path=}")

    for file in input_files:
        shutil.copy(file, output_path)

    sch = SCH(output_path / f"{project_name}.kicad_sch")
    symbol_instances = sch.symbol_instance_agg()
    sheet_instances = sch.sheet_instance_agg()
    sch.sheet_instance_clear()
    sch.symbol_instance_clear()
    sch.symbol_prop_dnp_clear()
    sch.sheet_prop_rename()
    sch.rectangle_remove()
    sch.version_change()
    sch.sheet_instances_set(sheet_instances)
    sch.symbol_instances_set(symbol_instances)
    logger.info("Schematic conversion complete.")

    pcb = PCB(output_path / f"{project_name}.kicad_pcb")
    pcb.version_change()
    pcb.fp_stroke_width_swap()
    logger.info("Board file conversion complete.")


if __name__ == "__main__":
    run()
