A utility for downgrading KiCad version 7 schematic and board files to KiCad version 6.

Created to serve as a temporary stop-gap to enable submission of designs to manufacturing/assembly partners that do not yet support KiCad version 7.

# Usage
Install dependencies listed in `pyproject.toml`, either manually using `pip`
or with `poetry` (recommended).

```shell
poetry install
poetry run python ./ki76.py -i <input_path> -o <output_path>
```
- `input_path`:  path to an existing v7 project directory or `kicad_pro` file.
- `output_path`: path to a directory that will be created and contain files converted to v6.

# Notes:
- Only `kicad_sch` and `kicad_pcb` files are converted.
- It can take a few seconds to convert the schematic and board files since updated copies of the files are saved after each operation (not optimal, but good enough for a limited-use utility such as this.)
- The `SCH` and `PCB` conversion classes may be used in an external script, if so desired.
- The included conversion operations are not exhaustive; loading a converted schematic or board file in KiCad 6 may result in a warning or error dialog window citing a file name and line number.  Open the named schematic or board file in a text editor, inspect the offending line, and extend `SCH` or `PCB` functionality as needed.
- When a project directory is provided as the input source, the `kicad_sch` file with a name that matches that of the directory is assumed to be the root schematic.
- All schematic and board files in the project directory are copied to the output directory, regardless of whether they are converted.  *Only schematic files referenced in the schematic heirarchy are converted.  Only the board file with a name matching that of the project is converted.*
- It is recommended that one use Gerber Viewer to compare corresponding gerber and drill files generated from the v7 and v6 board by loading corresponding layers, enabling "diff" or "exclusive-or" mode, and inspecting for differences.
- Similarly, BOMs generated from from v7 and v6 root schematics may be compared using a textual diff.
